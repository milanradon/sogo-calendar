import React from 'react';
import ReactDOM from 'react-dom';
import EventCalendar from './base';
import moment from 'moment';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Popover from 'react-bootstrap/lib/PopOver';
import Overlay from 'react-bootstrap/lib/Overlay';
import Modal from 'react-bootstrap/lib/Modal';
import TestData from './TestData.js';
import DateTimeField from 'react-bootstrap-datetimepicker'
import Select from 'react-select'

var dav = require('dav');
 
var xhr = new dav.transport.Basic(
  new dav.Credentials({
    username: 'balon',
    password: 'Sk89cp'
  })
);
 
dav.createAccount({ server: 'https://mail.uan.cz/SOGo/dav/balon/Calendar/personal/', xhr: xhr })
.then(function(account) {
  // account instanceof dav.Account
  account.calendars.forEach(function(calendar) {
    console.log('Found calendar named ' + calendar.displayName);
    // etc.x
  });
});

const people = [];
people.push({
    label: 'Anton Sverlov',
    value: 'Anton Sverlov'
});
people.push({
    label: 'lada Sverlov',
    value: 'lada Sverlov'
});

const hours = ["00", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "00"];

moment.locale('cs');
const now = moment();
class CalendarDemo extends React.Component {

    constructor(props) {
        super(props);
        // todo sogo people a getEvents
        const events = TestData.getEvents()
        const today = moment()
        let nowEventState = 'no'
        let minute = 0
        let nowEvents = events.filter(e =>
            moment(e.startDatetime) < today && moment(e.endDatetime) > today
        )
        if(nowEvents.length <= 0) {
            nowEvents = events.filter(e =>
                moment(e.startDatetime).format('DD:MM:YYYY') === moment(today).format('DD:MM:YYYY') && moment(e.startDatetime) > today
            )
            if(nowEvents.length > 0) {
                nowEvents = nowEvents.sort((a, b) => moment(a.startDatetime) < moment(b.startDatetime) ? 1 : -1)
                minute = moment(nowEvents[0].startDatetime) - moment(today)
                nowEventState = 'will running'
            }
        } else {
            nowEventState = 'running'
        }
        this.state = {
            moment: moment(),
            showPopover: false,
            showModal: false,
            overlayTitle: null,
            overlayMembers: [],
            overlayOrganizer: null,
            overlayStartDatetime: null,
            overlayEndDatetime: null,
            popoverTarget: null,
            overlayId: null,
            isAdd: false,
            user: 'Jednací místnost 1',
            events: events,
            nowEvent: nowEvents && nowEvents[0],
            nowEventState: nowEventState,
            minute: moment(minute).subtract(1, 'H').format('HH:mm'),
            day: null,
            dayEvents: null
        };

        this.handleNextMonth = this.handleNextMonth.bind(this);
        this.handlePreviousMonth = this.handlePreviousMonth.bind(this);
        this.handleToday = this.handleToday.bind(this);
        this.handleEventClick = this.handleEventClick.bind(this);
        this.handleEventMouseOver = this.handleEventMouseOver.bind(this);
        this.handleEventMouseOut = this.handleEventMouseOut.bind(this);
        this.handleDayClick = this.handleDayClick.bind(this);
        this.handleModalClose = this.handleModalClose.bind(this);
        this.handlePreviousDay = this.handlePreviousDay.bind(this);
        this.handleNextDay = this.handleNextDay.bind(this);
        this.handleChangeView = this.handleChangeView.bind(this);
    }

    handleChangeView() {
        const renderEvents  = this.actionDayView(moment(), this.state.events)
        this.setState({
            moment: moment(),
            showPopover: false,
            showModal: false,
            popoverTarget: null,
            day: this.state.day !== null ? null : 'day',
            dayEvents: renderEvents
        });
    }

    handleNextMonth() {
        this.setState({
            moment: this.state.moment.add(1, 'M'),
        });
    }

    handlePreviousMonth() {
        this.setState({
            moment: this.state.moment.subtract(1, 'M'),
        });
    }

    handleNextDay() {
        const renderEvents = this.actionDayView(this.state.moment.add(1, 'd'), this.state.events)
        this.setState({
            moment: this.state.moment.add(1, 'D'),
            dayEvents: renderEvents
        });
    }

    handlePreviousDay() {
        const renderEvents  = this.actionDayView(this.state.moment.subtract(1, 'd'), this.state.events)
        this.setState({
            moment: this.state.moment.subtract(1, 'D'),
            dayEvents: renderEvents
        });
    }

    handleToday() {
        this.setState({
            moment: moment(),
        });
    }

    handleEventMouseOver(target, eventData, day) {
        this.setState({
            showPopover: true,
            popoverTarget: () => ReactDOM.findDOMNode(target),
                overlayTitle: eventData.title,
                overlayMembers: eventData.members,
                overlayOrganizerTitle: eventData.organizer.label,
                overlayOrganizer: eventData.organizer,
                overlayStartDatetime: eventData.startDatetime,
                overlayEndDatetime: eventData.endDatetime,
                overlayId: eventData.id
        });
    }

    handleEventMouseOut(target, eventData, day) {
    }

    handleEventClick(target, eventData, day) {
        this.handleDayView(day)
    }

    handleDayClick(target, day) {
        this.handleDayView(day) 
    }

    actionDayView(m, events) {
        const dayEvents = events.filter(e =>
            (moment(e.startDatetime).format('YYYY:MM-DD') === moment(m).format('YYYY:MM-DD')) || (moment(e.startDatetime) < moment(m) && moment(m) < moment(e.endDatetime))
        )
        const sortEvents = dayEvents.sort((a, b) => moment(a.startDatetime) < moment(b.startDatetime) ? 1 : -1)
        const renderEvents = sortEvents.map(a => {
            let renderStart = a.startDatetime
            let renderEnd = a.endDatetime
            if(moment(a.startDatetime) < moment(m).startOf('day') ) {
                renderStart = moment(m).startOf('day')
            }
            if(moment(a.endDatetime) > moment(m).endOf('day') ) {
                renderEnd =  moment(m).endOf('day')
            }
            return {
                startDatetime: a.startDatetime,
                title: a.title,
                members: a.members,
                organizer: a.organizer,
                endDatetime: a.endDatetime,
                id: a.id,
                renderStart,
                renderEnd
            }
        })
        console.log(renderEvents)
        return renderEvents
    }

    handleDayView(day) {
        const m = moment(`${day.year}-${day.month+1}-${day.day}`);
        const renderEvents  = this.actionDayView(m, this.state.events)
        this.setState({
            showPopover: false,
            showModal: false,
            popoverTarget: null,
            moment: m,
            day: day,
            dayEvents: renderEvents
        });
    }

    getMomentFromDay(day) {
        return moment().set({
            'year': day.year,
            'month': (day.month + 0) % 12,
            'date': day.day
        });
    }

    handleModalClose() {
        this.setState({
            showModal: false,
        })
    }

    updateEvent() {
        // todo sogo
    }

    removeEvent() {
       // todo sogo
    }

    addEvent() {
       // todo sogo
    }

    showAddEvent() {
        this.setState({
            showPopover: false,
            showModal: true,
            overlayTitle: null,
            overlayMembers: [],
            overlayOrganizer: null,
            overlayStartDatetime: null,
            overlayEndDatetime: null,
            overlayId: null,
            isAdd: true
        });
    }

    showUpdateEvent() {
        this.setState({
            showPopover: false,
            showModal: true,
            isAdd: false
        });
    }

    getHumanDate() {
        return [moment.months('MM', this.state.moment.month()), this.state.moment.year() ].join(' ');
    }

    render() {

        const styles = {
            position: 'relative',
        };

        return (
            <div style={styles}>

                <div onClick={e => this.showAddEvent()} className="add-button-c">
                    <div className="add-button">+</div>
                </div>

                <Overlay
                    show={this.state.showPopover}
                    rootClose
                    onHide = {()=>this.setState({showPopover: false, })}
                    placement="top"
                    container={this}
                    target={this.state.popoverTarget}>
                    <Popover id="event">
                        <div className="h5">{this.state.overlayTitle}</div>
                        <div className="time">{moment(this.state.overlayStartDatetime).format('DD.MM dddd HH:mm')} - {moment(this.state.overlayEndDatetime).format('HH:mm')}</div>
                        <div className="organizator"><img src="./user.png"/>  Organizátor: <span>{this.state.overlayOrganizerTitle}</span></div>
                        <div className="members"><img src="./users.png"/>  Účastníci: {this.state.overlayMembers.map(a => <span>{a.label}</span>)}</div>
                        <div className="buttons">
                            <div className="update-button" onClick={e => this.showUpdateEvent()}>Upravit</div>
                            <div className="delete-button" onClick={e => this.removeEvent()}>Smazat</div>
                        </div>
                    </Popover>
                </Overlay>

                <Modal show={this.state.showModal} onHide={this.handleModalClose}>
                    <Modal.Footer>
                <input className="inputmodal" placeholder="Přidejte název" value={this.state.overlayTitle}  onChange={value => this.setState({ overlayTitle: value.target.event })} />
                <div className="twotimes">
                    <DateTimeField defaultText={this.state.overlayStartDatetime && moment(this.state.overlayStartDatetime).format('DD.MM.YYYY HH:mm')} inputFormat="DD.MM.YYYY HH:mm" onChange={value => this.setState({ overlayStartDatetime: value })} />
                    <DateTimeField defaultText={this.state.overlayEndDatetime && moment(this.state.overlayEndDatetime).format('DD.MM.YYYY HH:mm')} inputFormat="DD.MM.YYYY HH:mm" onChange={value => this.setState({ overlayEndDatetime: value })} />
                </div>
           
                    <div className="organizator a"><img src="./user.png"/>  Organizátor:
                        <Select
                        defaultValue={this.state.overlayOrganizer}
                        name="organizator"
                        options={people}
                        onChange={value => this.setState({ overlayOrganizer: value })}
                        className="basic-multi-select"
                        classNamePrefix="select"
                        placeholder="Přidat"
                        />
                    </div>
                        <div className="members"><img src="./users.png"/> Účastníci: 
                            <Select
                            isMulti
                            defaultValue={this.state.overlayMembers}
                            onChange={values => this.setState({ overlayMembers: values })}
                            name="members"
                            options={people}
                            className="basic-multi-select"
                            classNamePrefix="select"
                            placeholder="Přidat"
                            />
                        </div>
                        <div className="buttons">
                            <div className="update-button" onClick={e => this.handleModalClose()}>Zrušit</div>
                            <div className="add-modal-button" onClick={e => this.state.isAdd ? this.addEvent() : this.updateEvent()}><span>Uložit</span></div>
                        </div>
                    </Modal.Footer>
                </Modal>

                <br />
                <br />
                <br />
                <br />
                <Grid>
                    <Row>
                        <Col xs={4}>
  
                            <div style={{'display': this.state.day === null ? 'block': 'none'}}>   
                            <Button className="header-button-grey" onClick={this.handleToday}>{moment(now).format('DD.MM dddd HH:mm')}</Button>
                            <ButtonToolbar>
                                <Button className="header-button-arrow" onClick={this.handlePreviousMonth}>&lt;</Button>
                                <Button className="header-button">{this.getHumanDate()}</Button>
                                <Button className="header-button-arrow" onClick={this.handleNextMonth}>&gt;</Button>
                            </ButtonToolbar>
                            </div>
   
                            <div style={{'display': this.state.day !== null ? 'block': 'none'}}>   
                            <Button className="header-button-grey">{this.getHumanDate()}</Button>
                            <ButtonToolbar>
                                <Button className="header-button-arrow" onClick={this.handlePreviousDay}>&lt;</Button>
                                <Button className="header-button">{moment(this.state.moment).format('DD.MM dddd')}</Button>
                                <Button className="header-button-arrow" onClick={this.handleNextDay}>&gt;</Button>
                            </ButtonToolbar>
                            </div>
                    
                        </Col>
                        <Col xs={2}>
                            <div className="button-viewer" onClick={this.handleChangeView}>
                                <div className="viewer-title">{this.state.day === null ? 'Měsíc': 'Den'}</div>
                                <div className="arrow-down"></div>
                            </div>
                        </Col>
                       
                        <Col xs={4}>
                            {this.state.nowEvent && <div className="events-run">
                                {this.state.nowEventState === 'will running' &&
                                    <div className="events-run-title">Následujícící událost:</div>
                                }
                                {this.state.nowEventState === 'running'&& 
                                    <div className="events-run-title">Právě probíhá:</div>
                                }
                                <div className="events-run-name">{this.state.nowEvent.title}</div>
                                {this.state.nowEventState === 'will running' &&
                                    <div className="events-run-minute">za {this.state.minute}</div>
                                }
                            </div>}
                       
                        </Col>
                        <Col xs={2}>
                            <div className="pull-right h4">{this.state.user}</div>
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col xs={12}>
                        {this.state.day === null &&
                            <EventCalendar
                                month={this.state.moment.month()}
                                year={this.state.moment.year()}
                                events={this.state.events}
                                onEventClick={this.handleEventClick}
                                onEventMouseOver={this.handleEventMouseOver}
                                onEventMouseOut={this.handleEventMouseOut}
                                onDayClick={this.handleDayClick}
                                maxEventSlots={40}
                                style={{'display': this.state.day === null ? 'block': 'none'}}
                            />
                        }
                        <div style={{'display': this.state.day !== null ? 'block': 'none'}}>
                            {hours.map(h => <div style={{height: '40px'}}><span style={{
                                    position: 'relative',
                                    top: '30px'
                            }}>{h}</span> <hr style={{width: '95%'}}/></div>)}
                            {moment(this.state.moment).format('YYYY:MM-DD') === moment().format('YYYY:MM-DD') && 
                                <div className="timeline" style={{
                                    position: 'absolute',
                                    top: `${(parseInt(moment().format("HH") * 60) + parseInt(moment().format("mm")))/1.5 + 6}px`,
                                    width: '100%',
                                    height: '2px'
                                }}>
                                    <div className="dot"></div><div style={{'border-top': '1px solid white', 'border-bottom': '1px solid white', width: '100%', background: 'rgb(18, 173, 43)', height: '3px'}} ></div>
                                </div>
                             }
                            {this.state.dayEvents && this.state.dayEvents.map(e => <div>
                                {console.log(moment(e.renderStart).format('HH:MM'))}
                                {console.log(e.renderStart)}
                                {console.log((parseInt(moment(moment(e.renderEnd) - moment(e.renderStart)).format("HH") * 60)))}
                                <div onClick={er => this.handleEventMouseOver(er.target,e, null )} style={{
                                    'background': 'rgba(18, 173, 43, 1)',
                                    'color': 'white',
                                    'position': 'absolute',
                                    'width': '93%',
                                    'border-radius': '10px',
                                    left: '46px',
                                    'top': `${(parseInt(moment(e.renderStart).format("HH") * 60) + parseInt(moment(e.renderStart).format("mm")))/1.5 + 40}px`,
                                    'height': `${moment(e.renderStart).format('HH:mm') === "00:00" && moment(e.renderEnd).format('HH:mm') === "23:59" ? 960 : (parseInt(moment(moment(e.renderEnd) - moment(e.renderStart)).format("HH") * 60) + parseInt(moment(moment(e.renderEnd) - moment(e.renderStart)).format("mm")))/1.5 - 40}px`
                                    }}
                                >
                                    <span style={{
                                        padding: '5px',
                                        position: 'relative',
                                        top: '8px',
                                        left: '8px'
                                    }}>{moment(e.startDatetime).format('HH:mm')} {e.title}</span>
                                </div>
                            </div>)}
                        </div> 
                        </Col>
                    </Row>
                </Grid> 

            </div>
        );
    }
}

export default CalendarDemo;

