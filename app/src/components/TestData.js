import moment from 'moment';

export default {
    getEvents: () => {
        const now = moment();
        const dataFormat = 'YYYY-MM-DD';
        const eventMeta = [
            {
                start: 1,
                length: 5,
            },
            {
                start: 5,
                length: 1,
            },
            {
                start: 5,
                length: 3,
            },
            {
                start: 12,
                length: 15,
            },
            {
                start: 15,
                length: 45,
            },
            {
                start: 18,
                length: 6,
            },
            {
                start: 21,
                length: 5,
            },
            {
                start: 24,
                length: 14,
            },
            {
                start: 25,
                length: 9,
            },
        ]


        const events = eventMeta.map((data) => {
            const today = moment(now);
            const people = [];
            let i = 0;
            people.push({
                label: 'Anton Sverlov',
                value: 'Anton Sverlov'
            });
            people.push({
                label: 'lada Sverlov',
                value: 'lada Sverlov'
            });
            return {
                    id: i++,
                 start: today.date(data.start).format(dataFormat),
                 end: today.add(data.length-1, 'days').format(dataFormat),
                 startDatetime: moment(now).subtract(24, 'H'),
                 endDatetime: moment(now).add(12, 'H'),
                 eventClasses: 'custom-event-class',
                 title: data.length + ' day event ' + (data.title || ''),
                 organizer: {
                    label: 'Anton Sverlov',
                    value: 'Anton Sverlov'
                },
                 members: people    
            }
        }) 

        return events;
    }
};
