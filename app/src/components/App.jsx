import React from 'react';
import CalendarDemo from './CalendarDemo.jsx';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <CalendarDemo />
      </div>
    );
  }
}
